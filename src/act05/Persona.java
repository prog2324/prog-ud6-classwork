package act05;

public class Persona {
    private String nom;
    private String cognoms;
    private int edat;
    private boolean casat;

    public Persona(String nom, String cognoms, int edat, boolean casat) {
        this.nom = nom;
        this.cognoms = cognoms;
        this.edat = edat;
        this.casat = casat;
    }

    public Persona(String nom, String cognoms, int edat) {
        this.nom = nom;
        this.cognoms = cognoms;
        this.edat = edat;
        this.casat = false;
    }
    
    public void saluda() {
        String estoyCasado = (this.casat)? "SI": "NO";
        String saludo = "Hola, sóc %s %s, la meua edat és %d anys i %s estic casat\n";
        System.out.printf(saludo, this.nom, this.cognoms, this.edat, estoyCasado);
    }
    
}
