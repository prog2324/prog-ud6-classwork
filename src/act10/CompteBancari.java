package act10;

public class CompteBancari {
    private String nomClient;
    private String numCompte;
    private float tipusInteres;
    private float saldo;
    private boolean estaBloquejat;

    public CompteBancari(String nomClient, String numCompte, float tipusInteres, float saldo, boolean estaBloquejat) {
        this.nomClient = nomClient;
        this.numCompte = numCompte;
        this.tipusInteres = tipusInteres;
        this.saldo = saldo;
        this.estaBloquejat = estaBloquejat;
    }
    
    public CompteBancari(String nomClient, String numCompte) {
        this.nomClient = nomClient;
        this.numCompte = numCompte;
        this.tipusInteres = 0.1f;
        this.saldo = 0;
        this.estaBloquejat = false;
    }
    
    public boolean reintegra(float quantitat) {
        if (!this.estaBloquejat) {
            this.saldo = this.saldo - quantitat;
            return true;
        }
        return false;
    }
    
    public boolean ingressa(float quantitat) {
        if (!this.estaBloquejat) {
            this.saldo = this.saldo + quantitat;
            return true;
        }
        return false;
    }

    public boolean isEstaBloquejat() {
        return estaBloquejat;
    }    
    
    public String getNomClient() {
        return this.nomClient;
    }

    public String getNumCompte() {
        return numCompte;
    }

    public float getTipusInteres() {
        return tipusInteres;
    }

    public float getSaldo() {
        return saldo;
    }
    
    public void setSaldo(float saldo) {
        this.saldo = saldo;
    }
    
    public void setEstaBloquejat(boolean bloquejat) {
        this.estaBloquejat = bloquejat;
    }
    
    public void mostraInfo() {
       System.out.printf("Compte: %s, Client: %s, Interés: %.2f%%, Saldo: %.2f€\n",
               this.numCompte, this.nomClient, this.tipusInteres, this.saldo); 
    }
    
    public boolean transferirA(CompteBancari compteDesti, float quantitat) {
        if(this.isEstaBloquejat() || compteDesti.isEstaBloquejat()) {
            return false;
        }        
        this.saldo -= quantitat;
        compteDesti.setSaldo(compteDesti.getSaldo() + quantitat);
        return true;
    }
    
    
}
