package act10;

public class TestCompteBancari {

    public static void main(String[] args) {
        CompteBancari compte1 = new CompteBancari("Pepe Pérez", "00010021");
        compte1.ingressa(1000);
        compte1.reintegra(100);
        compte1.mostraInfo();

        CompteBancari compte2 = new CompteBancari("Juan Sánchez", "214521421", 0.5f, 30000, false);
        compte2.mostraInfo();
        compte2.setEstaBloquejat(false);
        if (compte2.reintegra(500)) {
            System.out.println("Reintegrament fet amb exit. Saldo actual: " + compte2.getSaldo() + " €");
        } else {
            System.out.println("No es posible fer reintegrament. Pareix que el compte está bloquejat");
        }
               
        // Añadido: transferir dinero de una cuenta a otra.
        System.out.println("Abans de transferencia...");
        compte1.mostraInfo();
        compte2.mostraInfo();
        
        // Opcion1: método transferir fuera de la clase.
        boolean ok = transferir(compte1, compte2, 100);
        if (ok) {
            System.out.println("Transferencia ok");
        }
        else {
            System.out.println("Error en transferencia");
        }
        
        // Opción2: método transfeir dentro de la clase
        ok = compte1.transferirA(compte2, 100);
        if (ok) {
            System.out.println("Transferencia ok");
        }
        else {
            System.out.println("Error en transferencia");
        }
        
        System.out.println("Després de transferencia...");
        compte1.mostraInfo();
        compte2.mostraInfo();

        System.out.println("Adeu!");

    }

    private static boolean transferir(CompteBancari compteOrige, CompteBancari compteDesti, float quantitat) {
       if (compteOrige.isEstaBloquejat() || compteDesti.isEstaBloquejat()) {
           return false;
       }
       compteOrige.setSaldo(compteOrige.getSaldo() - quantitat);
       compteDesti.setSaldo(compteDesti.getSaldo() + quantitat);
       return true;
    }
}
