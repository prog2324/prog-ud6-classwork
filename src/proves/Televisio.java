package proves;

class Televisio {
    // PROPIETATS
    private String marca;
    private int canal;
    private float volum;
    
    // CONSTRUCTORS
    public Televisio() {
        this.marca = "";
        this.canal = 2;
        this.volum = 2;
    }
    
    public Televisio(String marca) {
        this();
        this.marca = marca;
    }
    
    public Televisio(String marca, int canal) {
        this.marca = marca;
        this.canal = canal;
        this.volum = 1;
    }
    
    // COMPORTAMENT   
    public void establirCanal(int c) {
        canal = c;        
    }
    
    public void establirVolum(float v) {
        volum = v;
    }
    
    public void encendre() {
        System.out.println("Encendida!!");
        this.iluminarPixel(1, 123);
        // ...
    }
    
    private int iluminarPixel(int pixel, float rgb) {
        return 1;
    }
    
    public String obtenMarca() {
        return marca;
    }   
    
    public void imprimir() {
        System.out.println("Televisio{" + "marca " + marca + ", canal " + canal + ", volum " + volum + "}");
    }

}
