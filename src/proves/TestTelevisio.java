package proves;

public class TestTelevisio {
    public static void main(String[] args) {
        Televisio tele1 = new Televisio("Samsung");
        Televisio tele2 = new Televisio("Samsung");
        Televisio tele3 = new Televisio("LG", 9);
        
        tele1.encendre();
        tele1.establirVolum(10);
        tele1.establirCanal(4);
        
        System.out.println(tele1);
        
        tele1.imprimir();
        tele2.imprimir();
        tele3.imprimir();
        
    }
}
