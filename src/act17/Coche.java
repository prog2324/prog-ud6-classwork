package act17;

public class Coche {
   private String marca;
   private String modelo;
   private Tipo tipo;
   private TipoCambio tipoCambio;

    public Coche(String marca, String modelo, Tipo tipo, TipoCambio tipoCambio) {
        this.marca = marca;
        this.modelo = modelo;
        this.tipo = tipo;
        this.tipoCambio = tipoCambio;
    }
    
    public Coche(String marca, String modelo) {
        this.marca = marca;
        this.modelo = modelo;
        this.tipo = Tipo.DEPORTIVO;
        this.tipoCambio = TipoCambio.AUTOMATICO;        
    }
    
    public Coche(String marca, String modelo, Tipo tipo) {
        this.marca = marca;
        this.modelo = modelo;
        this.tipo = tipo;
        this.tipoCambio = TipoCambio.MANUAL;
        if (this.tipo == Tipo.DEPORTIVO) {
            this.tipoCambio = TipoCambio.AUTOMATICO;
        }        
    }

    @Override
    public String toString() {
        return "Coche [" + marca + " " + modelo + ", " + tipo + ", " + tipoCambio + "]";
    }
   
   
}
