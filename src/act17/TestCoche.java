package act17;

public class TestCoche {

    public static void main(String[] args) {
        Coche[] listaCoches = new Coche[5];

        Coche c = new Coche("Seat", "Ibiza", Tipo.TURISMO, TipoCambio.MANUAL);
        listaCoches[0] = c;
        listaCoches[1] = new Coche("Ford", "Fiesta", Tipo.TURISMO, TipoCambio.MANUAL);
        listaCoches[2] = new Coche("Renault", "Clio", Tipo.DEPORTIVO);
        listaCoches[3] = new Coche("Land Rover", "Defender", Tipo.TODO_TERRENO);
        listaCoches[4] = new Coche("Audi", "R8");
        
        for(Coche e: listaCoches) {
            System.out.println(e);
        }

    }
}
