package act17;

public enum TipoCambio {
    AUTOMATICO {
        @Override
        public String toString() {
            return "[Aut]";
        }        
    }, 
    MANUAL {
        @Override
        public String toString() {
            return "[Man]";
        }
    }
}
