package act03_6_7;

public class Rectangle {

    private int amplaria;
    private int alcada;
    private static int contador;

    public Rectangle() {
        this.amplaria = 1;
        this.alcada = 1;
        contador++;
    }

    public Rectangle(int amplaria, int alcada) {
        this.amplaria = amplaria;
        this.alcada = alcada;
        contador++;
    }

    public int getAmplaria() {
        return amplaria;
    }

    public void setAmplaria(int amplaria) {
        this.amplaria = amplaria;
    }

    public int getAlcada() {
        return alcada;
    }

    public void setAlcada(int alcada) {
        this.alcada = alcada;
    }

    public static int getContador() {
        return contador;
    }

    public int area() {
        int area = this.amplaria * this.alcada;
        return area;
    }

    public void imprimir() {
        System.out.println(this.amplaria + " x " + this.alcada);
    }

    public void mostrarInfo() {
        System.out.println("** Rectángulo **");
        System.out.println("\tAltura: " + this.alcada + " m");
        System.out.println("\tAnchura: " + this.amplaria + " m");
        System.out.println("\tArea: " + this.area() + " m2");
    }
}
