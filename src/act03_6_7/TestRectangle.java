package act03_6_7;

public class TestRectangle {

    public static void main(String[] args) {
        Rectangle r1 = new Rectangle(100, 200);
        Rectangle r2 = new Rectangle(40, 100);
        Rectangle r3 = new Rectangle(80, 40);
        Rectangle r4 = new Rectangle(56, 90);
        Rectangle r5 = new Rectangle(100, 150);
        Rectangle r6 = new Rectangle(200, 120);

//        int a1 = r1.area();
//        System.out.print("Rectángulo r1: ");
//        r1.imprimir();
//        System.out.println("\tArea r1: " + a1);
//
//        int a5 = r5.area();
//        System.out.print("Rectángulo r5: ");
//        r5.imprimir();
//        System.out.println("\tArea r5: " + a5);
//
//        System.out.print("Rectángulo r2: ");
//        r2.imprimir();
//        System.out.print("Rectángulo r3: ");
//        r3.imprimir();
//        System.out.print("Rectángulo r4: ");
//        r4.imprimir();
//        System.out.print("Rectángulo r6: ");
//        r6.imprimir();
        
        System.out.println("La altura de r1 es: " + r1.getAlcada());
        System.out.println("La anchura de r1 es: " + r1.getAmplaria());
        
        Rectangle r = new Rectangle();
        r.setAmplaria(120);
        r.setAlcada(215);
        r.imprimir();
        
        // llamando al método de test
        mostrarInfo(r);
        
        // llamando al método de la clase
        r.mostrarInfo();
        
        // probando variable estática
        System.out.println(Rectangle.getContador());
        Rectangle r0 = new Rectangle();
        System.out.println(r1.getContador()); // mejor no
        System.out.println(Rectangle.getContador());
        
        // creando array de rectangulos
        Rectangle[] rectangulos = new Rectangle[10];
        rectangulos[0] = new Rectangle(100, 20);
        rectangulos[1] = r1;
        rectangulos[0].mostrarInfo();
        
    }

    private static void mostrarInfo(Rectangle r) {
        System.out.println("** Rectángulo **");
        System.out.println("\tAltura: " + r.getAlcada() + " m");
        System.out.println("\tAnchura: " + r.getAmplaria() + " m");
        System.out.println("\tArea: " + r.area() + " m2");
    }
}
