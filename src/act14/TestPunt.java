package act14;

public class TestPunt {
    public static void main(String[] args) {
        Punt p1 = new Punt(0, 0);
        Punt p2 = new Punt(5, 3);
        Punt p3 = new Punt(2, -1);
        Punt p4 = p2.getPuntMitja(p3);
        Punt p5 = new Punt(4, 3);
        
        System.out.printf("Distancia entre %s y %s es: %.2f\n", 
                p5, p1, p1.getDistancia(p5));
        System.out.println(p1);
        System.out.println(p2);
        System.out.println(p3);
        System.out.println(p4);
        System.out.println(p5);
        
        
    }
}
