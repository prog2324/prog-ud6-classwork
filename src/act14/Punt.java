package act14;

public class Punt {

    private int x;
    private int y;

    public Punt(int x, int y) {
        this.x = x;
        this.y = y;
    }
    
    public double getDistancia(Punt punt) {
        return Math.sqrt(Math.pow(punt.x - this.x, 2) 
                + Math.pow(punt.y - this.y, 2));
    }
    
    public Punt getPuntMitja(Punt punt) {
        int mitatX = (this.x + punt.x) / 2;
        int mitatY = (this.y + punt.y) / 2;
        return new Punt(mitatX, mitatY);
    }

    @Override
    public String toString() {
        return "(x=" + this.x + ",y=" + this.y + ")";
    }
}
